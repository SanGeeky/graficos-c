#include <winbgim.h>
#include <iostream>
#include <stdlib.h>
#include "grafico.h"

float* x = NULL;
float* y = NULL;
int windowx,windowy;

void  pixel() {
  int x,y;
  std::cout << "Ingrese Punto en X: " ; std::cin >> x;
  std::cout << "\nIngrese Punto en Y: " ; std::cin >> y;

  initwindow(windowx,windowy);
  pixel(x,y);
}

void  line() {
  float x1,y1,x2,y2;
  std::cout << "Ingrese (X1) : " ; std::cin >> x1;
  std::cout << "\nIngrese (Y1) : " ; std::cin >> y1;
  std::cout << "\nIngrese (X2) : " ; std::cin >> x2;
  std::cout << "\nIngrese (Y2) : " ; std::cin >> y2;

  initwindow(windowx,windowy);
  linea(x1,y1,x2,y2);
}

void square() {
  float x,y,lado;
  std::cout << "Ingrese (X) : " ; std::cin >> x;
  std::cout << "\nIngrese (Y) : " ; std::cin >> y;
  std::cout << "\nIngrese Lado: " ; std::cin >> lado;

  initwindow(windowx,windowy);
  cuadrado(x,y,lado);
}

void  rectangle() {
  float x1,y1,x2,y2;
  std::cout << "Ingrese (X1) : " ; std::cin >> x1;
  std::cout << "\nIngrese (Y1) : " ; std::cin >> y1;
  std::cout << "\nIngrese (X2) : " ; std::cin >> x2;
  std::cout << "\nIngrese (Y2) : " ; std::cin >> y2;

  initwindow(windowx,windowy);
  rectangulo(x1,y1,x2,y2);
}

void  triangle() {
  float x1,y1,x2,y2,x3,y3;
  std::cout << "Ingrese (X1) : " ; std::cin >> x1;
  std::cout << "\nIngrese (Y1) : " ; std::cin >> y1;
  std::cout << "\nIngrese (X2) : " ; std::cin >> x2;
  std::cout << "\nIngrese (Y2) : " ; std::cin >> y2;
  std::cout << "\nIngrese (X3) : " ; std::cin >> x3;
  std::cout << "\nIngrese (Y3) : " ; std::cin >> y3;

  initwindow(windowx,windowy);
  triangulo(x1,y1,x2,y2,x3,y3);
}

void polygon() {

//  **************************poligono*****************
	int n;

    do {
      std::cout << "Puntos a Ingresar: ";
      std::cin >> n;
    } while(n<3);

    x = new float[n]; y = new float[n];

    for (int i=0; i<n; i++)
    {
      std::cout << '\n' << "Ingresar Punto " << i+1 <<" (x,y)" <<'\n';
      std::cin >> x[i] >> y[i];
    }
    // Revisar Puntos
    // for (int i=0; i<n; i++)
    // {
    //   std::cout << '\n' << x[i] << y[i] <<'\n';
    // }
    initwindow(windowx,windowy);
    poligono(x,y,n);

    delete [] x; delete [] y;

}


void menu()
{
	int option;
	do
	{
		do
		{
			system("CLS");
			cout <<" 			Figuritas 				";
			cout <<"\n================================================================================";
			cout<< "\n"
				<< "\n1. Pixel "
				<< "\n2. Line "
				<< "\n3. Square "
				<< "\n4. Rectangle "
				<< "\n5. Triangle  "
				<< "\n6. Polygon "
        << "\n0. Exit "
				<< "\nChoose Drawing Option: ";
			cin>> option;
    }while(option<0 || option>6);
	        system("CLS");
			switch(option)
			{
				case 1:
						system("CLS");
						pixel();
						system("PAUSE");
            closegraph();
				    break;

				case 2:
              system("CLS");
					    line();
              system("PAUSE");
              closegraph();
					    break;

        case 3: system("CLS");
					     square();
               system("PAUSE");
               closegraph();
					    break;

				case 4: system("CLS");
						rectangle();
            system("PAUSE");
            closegraph();
				        break;
				 case 5:
						system("CLS");
						triangle();
            system("PAUSE");
            closegraph();
						break;

        case 6: system("CLS");
             polygon();
             system("PAUSE");
             closegraph();
                break;

				case 0:	system("CLS");
						cout << "\n\n\n Bye Bye! \n\n\n";
				    break;
			}

  }while (option!=0);
}



int main()
{
  std::cout << "Ingrese Tamanio del Lienzo:" << '\n';
  std::cin >>  windowx >> windowy;
  menu();
  system("PAUSE");
	return 0;
}
