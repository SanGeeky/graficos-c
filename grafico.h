#include <winbgim.h>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <iomanip>
using namespace std;


void pixel(int x, int y){
	int color = 5;
	putpixel(x,y,color);
}

void linea(float x1, float y1, float x2, float y2) {
	float xi,xf,yi,yf;
	float pendiente;
	float b;


	/////HORIZONTAL IZQ - DER  x1 < x2  y1 = y2
	/////HORIZONTAL DER - IZQ  x1 > x2  y1 = y2

	if(x1 != x2)
	{
		pendiente = (y2-y1)/(x2-x1);
	}

	//////////////// X
	if (x1 < x2 ) {
		xi=x1; xf=x2;
	}
	else if (x1 > x2) {
		xi=x2; xf=x1;
	}
	else
	{
		xi=x1; xf=x2;
	}
	// /////////////////// Y
	if (y1 < y2 ) {
		yi=y1; yf=y2;
	}
	else if (y1 > y2) {
		yi=y2; yf=y1;
	}
	else
	{
		yi=y1; yf=y2;
	}

	if(xi == xf)
	{
		for (int i = yi; i <= yf; i++) {
			pixel(xi,i);
		}
	}
	////cout << fixed << setprecision(2) << pendiente << " pendiente \n";
  if(pendiente >= 0)
	{
			b = yi - (pendiente*xi);
			for (int i = xi; i <= xf; i++) {
				pixel(i,pendiente*i+b);
			}
	}
	else
	{
		pendiente = (y2-y1)/(x2-x1);
		if(x1 > x2)
		{
			xi=x2; xf=x1; yi=y2; yf=y1;
		}
		else
		{
			xi=x1; xf=x2; yi=y1; yf=y2;
		}
	  b = yi - (pendiente*xi);
		//cout << b << "\n " ;
		//cout << pendiente << " pendiente \n";
		for (int i = xi; i <= xf; i++)
		{
			pixel(i,(pendiente*i)+b);
		}
	}
}

void  cuadrado(float xi, float yi, float lado) {
	///Lado 1
	linea(xi,yi,xi+lado,yi);
	// lado 2
	linea(xi+lado,yi,xi+lado,yi+lado);
	// lado 3
	linea(xi+lado,yi+lado,xi,yi+lado);
	// lado 4
	linea(xi,yi+lado,xi,yi);
}

void rectangulo(float x1, float y1, float x2, float y2)
{
	//lado 1
	linea(x1,y1,x2,y1);
	//lado 2
	linea(x2,y1,x2,y2);
	//lado 3
	linea(x2,y2,x1,y2);
	// lado 4
	linea(x1,y2,x1,y1);
}

void triangulo(float x1, float y1, float x2, float y2, float x3, float y3)
{
	// lado 1
	linea(x1,y1,x2,y2);
	// lado 2
	linea(x2,y2,x3,y3);
	// lado 3
	linea(x3,y3,x1,y1);

}

void poligono(float x[], float y[], int n) {
	for (int i = 0; i<=n; i++)
	{
		if (i==n-1) {
			//////cout << x[0] << y[0] << "--" << x[i] << y[i] ;
			linea(x[0],y[0],x[i],y[i]);
		}
		else
			linea(x[i],y[i],x[i+1],y[i+1]);
	}
}
